import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
       <div className="App-wrap">
          <div className="App-root">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <p>
               莫欺少年穷
              </p>
            </header>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
